# Explain Syntax Definition

Pandoc and KDE-style XML syntax definition file for PostgreSQL explain plans.


## Use with pandoc

1. Put you Postgres explain plan in a code section with the tag `explain` :

~~~markdown

    ```explain 
                        QUERY PLAN                                                                                            
    ---------------------------------------------------
    Delete on emailmessages  (cost=224.85..38989.92 rows=5000 width=34)
        Buffers: shared hit=2579331 read=506594 dirtied=503671
        ->  Nested Loop  (cost=224.85..38989.92 rows=5000 width=34) 
    ```
~~~

2. use the `--syntax-definition` option of pandoc

~~~bash
$ pandoc foo.md --standalone --syntax-definition=explain.xml -o foo.html
~~~